var pushNotification;
var controller  = window.angular.element(document.getElementById('controller'));
function onDeviceReady() {
    console.log("deviceready event received");
    try {
        pushNotification = window.plugins.pushNotification;
        console.log("registering " + device.platform);
        if (device.platform == 'android' || device.platform == 'Android' ||
            device.platform == 'amazon-fireos') {
            pushNotification.register(successHandler, errorHandler, {
                "senderID": "60357548107",
                "ecb": "onNotification"
            });
        }
    } catch (err) {
        console.log("There was an error on this page.");
        console.log("Error description: " + err.message);
    }
}

// handle GCM notifications for Android
function onNotification(e) {
    console.log("EVENT -> RECEIVED:" + e.event);
    switch (e.event) {
        case 'registered':
            if (e.regid.length > 0) {
                console.log("<li>REGISTERED -> REGID:" + e.regid);
                controller.scope().setGCMID(e.regid);
            }
        break;

        case 'message':
            controller.scope().notifications(e);
        case 'error':
            //console.log(e.msg);
        break;
        default:
            console.log("Unexpected event.");
        break;
    }
}

function tokenHandler(result) {
    console.log("token: " + result);
}

function successHandler(result) {
    console.log("success:" + result);
}

function errorHandler(error) {
    console.log("error: " + error);
}

document.addEventListener('deviceready', onDeviceReady, true);