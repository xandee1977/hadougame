var CacheControl = {
    litefime: 60, // default time life in seconds
    cache_keys : [],
    cache_result : [],

    // Generate a key for cache params
    save_key : function(params) {
        var result = false;
        try {
            this.cache_keys.push(params);
            result = this.get_key(params);
        } catch(e) {
            console.log(e);
        }
        return result;
    },

    get_key : function(params) {
        var result = false;
        try {
            params = this.stringfy(params);
            var index = _.findIndex(this.cache_keys, params);
            
            if(index !== -1) {
                result = index;
            }
        } catch(e) {
            console.log(e);
        }
        return result;
    },

    // Add result to cache
    save_cache: function(params, ws_result, lifetime) {
        var result = false;
        try {
            var cache = this.get_cache(params);
            if(cache !== false) {
                throw "[ save_cache ] - cache saved yet ...";
            }
            // saving the key
            var cache_key = this.save_key(params);
            if(cache_key === false) {
                throw "[ save_cache ] - error on saving cache ...";
            }
            
            // get the default lifetime
            if( typeof lifetime === "undefined" ) {
                var lifetime = this.litefime;
            }            

            // Saving the cache
            var cache = {};
            cache.data = ws_result;
            cache.request = params;
            cache.status = "OK";
            cache.message = "Dale!";
            cache.time = Math.floor(Date.now() / 1000);
            cache.lifetime = lifetime;  // Timelife of this cache
            
            this.cache_result[cache_key] = cache;
            return cache;
        } catch(e) {
            console.log(e);
        }

        return result;
    },

    // Add result to cache
    get_cache: function(params) {
        var result = false;
        var cache_key =  false;
        try { 
            cache_key = this.get_key(params);
            if( cache_key === false) {
                throw "[ get_cache_data ] - cache key not found ...";
            }
            
            var cache = this.cache_result[cache_key];
            if( typeof cache === "undefined" ) {
                throw "[ get_cache_data ] - error on get cache ...";   
            }
            
            
            // Expirating cache when trespassing timelife
            var cache_time = (Math.floor(Date.now() / 1000) - cache.time);
            console.log("cache time left: " + String(cache.lifetime - cache_time) );
            if( cache_time > cache.lifetime){
                // Cleaning
                this.cache_result.splice(cache, 1);
                this.cache_keys.splice(cache_key, 1);
                throw "[ get_cache_data ] - expiring cache ...";
            }
            
            result = cache;
        } catch(e) {
            console.log(e);
        }
        return result;
    },

    stringfy: function(params) {
        var result = {};
        for(key in params){
            result[key] = String(params[key]);
        }
        return result;
    }
};