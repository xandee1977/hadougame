GameApp.controller('GameController', function($scope, $http, $anchorScroll, $location) {
    $scope.menu_type = "burger";
    $scope.base_url = "http://beecoapp.com/ws-game/";
    $scope.user_id = localStorage.getItem('user_id') || null;
    $scope.system_id = localStorage.getItem('system_id') || 1;
    $scope.friend = false;
    $scope.bar_title = "";
    $scope.error_message = false;
    $scope.gcm_id = localStorage.getItem('gcm_id') || null;


    if(!localStorage["profile_data"]) {
        $scope.profile_data = null;
    } else {
        $scope.profile_data = JSON.parse(localStorage["profile_data"]) || null;
    }

    $scope.params = {};

    $scope.systems = [
        {"id": 2, "name": "Atari 2600" },
        {"id": 16, "name": "Dreamcast" },
        {"id": 17, "name": "Gameboy" },
        {"id": 18, "name": "Gameboy Color" },
        {"id": 19, "name": "Gameboy Advance" },
        {"id": 9, "name": "Master System" },
        {"id": 3, "name": "Mega Drive" },
        {"id": 22, "name": "Neo Geo" },
        {"id": 21, "name": "Nintendo 3DS" },
        {"id": 4, "name": "Nintendo 64" },
        {"id": 8, "name": "Nintendo (8 Bits)" },
        {"id": 20, "name": "Nintendo DS" },
        {"id": 14, "name": "Nintendo Wii" },
        {"id": 1, "name": "Super Nintendo" },
        {"id": 5, "name": "Playstation" },
        {"id": 6, "name": "Playstation 2" },
        {"id": 7, "name": "Playstation 3" },
        {"id": 10, "name": "Playstation 4" },
        {"id": 24, "name": "PSP" },
        {"id": 23, "name": "PS Vita" },
        {"id": 15, "name": "Sega Saturn" },
        {"id": 11, "name": "Xbox" },
        {"id": 12, "name": "Xbox 360" },
        {"id": 13, "name": "Xbox One" }
    ]
    
    // Historico de navegacao
    $scope.navigation = [];    
    $scope.FriendControlId = [];
    $scope.chat_messages = []; 
    $scope.topList = [];
    $scope.controlId = [];
    $scope.gameList = [];
    $scope.states = [];
    $scope.cities = [];
    $scope.listSellers = [];
    $scope.hadougamers = [];

    $scope.loading = false;
    $scope.popupSellers = false;
    $scope.current_view = null; // View inicial
    $scope.current_action = null;

    // Display the login screen
    $scope.actionChat = function(friend, menu_type, limit) {
        console.log("Menu Type...");
        console.log(menu_type);

        if(typeof menu_type == "undefined") {
            $scope.menu_type = "arrow";
        }        
        $scope.friend = friend;
        //$scope.bar_title = "Chat";
        var action = "chat";
        if($scope.current_action != action) {
            window.closeMenu();
            $scope.getChatMessageList($scope.friend.user_id, limit);
            $scope.current_view = "views/chat.html";
        }
    }
    // Sends chat message
    $scope.send_chat_message = function(receiver_id, message) {
        try {
            var temp_id = Math.random().toString(36).slice(2);
            var temp_message = {
                "message_id":temp_id,
                "sender_id":$scope.user_id,
                "receiver_id":receiver_id,
                "message": message,
                "origin": "yours",
                "date_time": "enviando ..."
            };
            $scope.addChatMessage(temp_message);
            $("#chat_message_text").val(""); // Celanning the field

            var params = {};
            params.service = "social";
            params.action = "add-chat-message";
            params.sender_id = String($scope.user_id);
            params.receiver_id = String(receiver_id);
            params.message = String(message);
            params.message_index = $scope.chat_messages.indexOf( temp_message );
            
            $scope.ws_get_request(params, "send_chat_message");
        } catch(e) {
            console.log(e);
        }
    }    

    $scope.addChatMessage = function(data) {
        console.log("addChatMessage");
        $scope.chat_messages.push(data);
        
        // Jump to the last element on the list
        $location.hash("message-" + String(data.message_id));
        $anchorScroll();

        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
            $scope.$apply();            
        }
    }

    $scope.addListChatMessage = function(list_messages) {
        for(var i=0;  i<list_messages.length; i++) {
            $scope.addChatMessage(list_messages[i]);
        }
    }

    // Getting chat message list
    $scope.getChatMessageList = function(friend_id, limit) {
        $scope.chat_messages = []; // Cleaning the chat
        $scope.loading=true;
        var params = {};
        params.service="social";        
        params.action="list-chat-conversation";
        params.user_id= String($scope.user_id);
        params.friend_id= String(friend_id);
        if(typeof limit != "undefined") {
            params.limit= limit;
        } 

        $scope.ws_get_request(params, "chat_message_list");
    }

    // Search trigger
    $scope.setGCMID = function(gcm_id) {
        console.log("setGCMID");
        if($scope.user_id && (!$scope.gcm_id || !$scope.gcm_id == null || !$scope.gcm_id == "")) {
            $scope.doSaveGCM($scope.user_id, gcm_id);
        }
    }

    // Save an user
    $scope.doSaveGCM = function(user_id, gcm_id) {
        console.log("doSaveGCM");
        try {
            var params = {};
            params.service = "user";
            params.action = "save";
            params.cgm_id = String(gcm_id);
            var data = {"user_id": String(user_id), "user_gcm_id": String(gcm_id)};
            $scope.ws_post_request(params, data, "save_gcm");
        } catch(e) {
            console.log(e);
        }
    };

    // Store the navigation history
    $scope.navigation_store = function(params) {
        //console.log("navigation_store");
        if($scope.navigation.length > 5) {
            // Remove o primeiro elemento
            $scope.navigation.shift();
        }
        $scope.navigation.push(params);
    }

    // Back to previous history element
    $scope.history_back = function() {
        $scope.menu_type = "burger";        
        var params = $scope.navigation[$scope.navigation.length-1];
        $scope.friend = params.friend == "false" ? false : params.friend;                
        $scope.current_action = params.current_action;

        $scope.ws_get_request(params, params.requestType);        
        $scope.current_view = params.current_view;
    }

    // Format url
    $scope.get_url = function() {
        var url = $scope.base_url;
        // Remove null falues
        for (var key in $scope.params) {
           if ($scope.params[key] == null || $scope.params[key] == "null") {
              delete $scope.params[key];
           }
        }
        var qs = $.param($scope.params);        
        console.log(url + "?" + qs);
        return url + "?" + qs;
    }

    // Do GET requests
    $scope.ws_get_request = function(params, requestType) {
        $scope.error_message = false;
        $scope.params = params;
        $scope.params.requestType = requestType;
        $scope.params.current_action = $scope.current_action;
        $scope.params.callback = "request_callback";
        
        var url = $scope.get_url();

        var cache = window.CacheControl.get_cache($scope.params);
        if(cache !== false) {
            console.log("Loading from cache...");
            // Using cached data
            window.action_data(cache);
        } else {
            console.log("Loading from web...");
            $http.jsonp(url).then(
                function(s) { 
                    $scope.success = JSON.stringify(s);
                    $scope.loading = false;
                }, 
                function(e) { 
                    $scope.error = JSON.stringify(e);
                    $scope.loading = false;
                }
            );
        }
    }

    // Do POST requests
    $scope.ws_post_request = function(params, data, requestType) {
        $scope.error_message = false;
        $scope.params = params;
        $scope.params.requestType = requestType;
        $scope.params.current_action = $scope.current_action;

        var url = $scope.get_url();
        
        $http({
            method: 'POST',
            url: url,
            data: data,
            headers: {'Content-Type': 'application/json; charset=utf-8'}
        })
        .then(function(response) {                
            window.request_callback(response.data);
            $scope.loading = false;
        }, 
        function(response) {
            // Error
            console.log("Error ws_post_request");
            $scope.loading = false;
        });
    }

    // Logouting the system
    $scope.doLogout = function() {
        $scope.current_action = 'logout';
        $scope.bar_title = "";
        $scope.params.user_id = null;
        $scope.profile_data = null;
        localStorage.setItem('user_id', null);
        localStorage.setItem('profile_data', null);
        localStorage.setItem('gcm_id', null);
        $scope.user_id = null;
        $scope.gcm_id = null;
        $scope.actionLogin();
    }

    // Display the login screen
    $scope.actionLogin = function() {
        var action = "login";
        if($scope.current_action != action) {
            window.closeMenu();
            $scope.current_view = "views/login.html";
        }
    }

    // Doing login
    $scope.doLogin = function() {
        $scope.email = $("#login_email").val();
        $scope.password = CryptoJS.MD5($("#login_password").val());
        
        var params = {};
        params.service="user";
        params.action="login";
        params.email= String($scope.email);
        params.password= String($scope.password);

        $scope.ws_get_request(params, "do_login");
    }    

    // Save the profile
    $scope.doSaveProfile = function() {
        $scope.current_action = 'profile';
        var profile_id = $("#profile_id").val();
        var nickname = $("#nickname").val();
        var state_id = $("#state_id ").val();
        var city_id = $("#city_id").val();
        var about_me = $("#about_me").val();

        var data = {
            "user_id": $scope.user_id,
            "nickname": nickname,
            "state_id": state_id,
            "city_id": city_id,
            "resume": about_me
        };
        // Adiciona o id do profile se houver
        if(profile_id != "") {
            data["profile_id"] = profile_id;
        };

        var params = {};
        params.service="user";
        params.action="save-profile";
        params.user_id=String($scope.user_id);
        
        $scope.ws_post_request(params, data, "save_profile");
    }

    // Send feedback message
    $scope.doSendFeedback = function() {
        $scope.current_action = 'feedback';

        var subject = $("#fb-subject").val();
        var message = $("#fb-message").val();
        var data = {};
        data.subject=JSON.stringify(subject);
        data.message=JSON.stringify(message);

        var params = {};
        params.service="user";
        params.action="feedback";
        params.user_id=String($scope.user_id);

        $scope.ws_post_request(params, data, "send_feedback");
    }

    $scope.addToGameList = _.memoize(function(game_list) {
        for(var i=0;  i<game_list.length; i++) {
            if($scope.controlId.indexOf(game_list[i].game_id) == -1) {
                $scope.gameList.push(game_list[i]);
            }
            $scope.controlId.push(game_list[i].game_id);
        }        
    }, function(){return Math.floor(Date.now() / 1000); });

    $scope.addToFriendList = function(friend_list) {
        for(var i=0;  i<friend_list.length; i++) {
            if($scope.FriendControlId.indexOf(friend_list[i].user_id) == -1) {
                $scope.hadougamers.push(friend_list[i]);
            }
            $scope.FriendControlId.push(friend_list[i].user_id);
        }        
    }

    // Get the action list
    $scope.actionList = function(user_id) {
        // Logica do modulo
        var action = "list";
        if($scope.current_action != action) {
            $scope.current_action = action
            window.closeMenu();
            var params = {};
            
            params.user_id = $scope.user_id;
            if(typeof user_id != "undefined") {
                params.user_id = user_id;
            }

            $scope.system_id = localStorage.getItem('system_id');
            
            // Limpa os filtros
            params.service = "game";
            params.action = "list";
            params.search = null;
            params.flag = null;
            params.system = $scope.system_id || localStorage.getItem('system_id');
            params.current_view = "views/game-list.html";
            
            // Cleaning the list
            $scope.gameList = [];
            $scope.controlId = [];
            
            $scope.ws_get_request(params, "list_games");
            $scope.current_view = params.current_view;
        }
    }

    // Display the use terms of Hadougame
    $scope.doActionTerms = function() {
        $scope.bar_title = "Termos de uso";
        var action = "terms";

        if($scope.current_action != action) {
            $scope.current_action = action;
            window.closeMenu();

            var params = {};
            params.service="social";
            params.action="terms";
            params.current_view = "views/use-terms.html";
            $scope.ws_get_request(params, "use_terms");
            $scope.current_view = params.current_view;
        }        
    }

    $scope.doGetSystemGames = function(system) {
        var action = "changeSystem";
        var system = parseInt(system);
        if($scope.current_action != action) {
            $scope.gameList = [];

            $scope.current_action = action;

            // Modifica o systema
            localStorage.setItem('system_id', system);
            $scope.params.system = system;
            $scope.actionList();
        }
    }

    $scope.moreGames = function() {
        var params = {};
        var limit1 = 0;
        var limit2 = 10;
        if($scope.gameList.length > 0) {
            limit1 = $scope.gameList.length-1;
        }
        // Limpa os filtros
        params.limit = limit1 + "," + limit2;
        params.service = "game";
        params.action = "list";
        params.user_id = $scope.params.user_id || null;
        params.search = $scope.params.search || null;
        params.flag = $scope.params.flag || null;
        params.system = $scope.system_id;
        if($scope.friend) {
            params.system=0; // Clean system for friend
            params.user_id=$scope.friend.user_id;
            params.only_owner_sell=$scope.params.only_owner_sell;
        }

        params.display_messages = 0; // Dont display alerts
        params.current_view = "views/game-list.html";

        $scope.ws_get_request(params, "more_games");
        $scope.current_view = params.current_view;
    }

    $scope.doSearchByFlag = function(flag) {
        $scope.loading=true;
        $scope.current_action = 'list';        
        window.closeMenu();
        // Disabilita os botoes
        disabledFlagButtons();

        var params = {};
        params.limit = "0,10";
        params.service = "game";
        params.action = "list";
        params.flag = flag;
        params.search = $scope.params.search;
        params.system = $scope.system_id;
        
        params.user_id = $scope.user_id;
        if($scope.friend) {
            params.system=0; // Clean system for friend
            params.user_id=$scope.friend.user_id;
            params.only_owner_sell=1;
        }
        params.current_view = "views/game-list.html";

        if($scope.friend) {
            params.flag = flag;
            $("#bt-" + flag).attr("class", "bt-header bt-" + flag + "-enabled");
        } else {
            if($scope.params.flag == flag) {
                params.flag = null;
                $("#bt-" + flag).attr("class", "bt-header bt-" + flag);
            } else {
                params.flag = flag;
                $("#bt-" + flag).attr("class", "bt-header bt-" + flag + "-enabled");
            }
        }
        
        // Cleaning the list
        $scope.gameList = [];
        $scope.controlId = [];

        $scope.ws_get_request(params, "list_games");
        $scope.current_view = params.current_view;
    }

    if($scope.user_id == null || $scope.user_id == "null") {
        $scope.actionLogin();
    } else {
        $scope.doGetSystemGames($scope.system_id);
        //$scope.actionChat();
    }

    // Get the action profile
    $scope.actionProfile = function() {
        // Logica do modulo
        $scope.bar_title = "Perfil";
        var action = "profile";
        if($scope.current_action != action) {
            window.closeMenu();
            $scope.current_action = action;
            $scope.doGetStates();// Losds ths states list
            $scope.current_profile_image = "img/camera.gif";
            if(typeof $scope.profile_data != "undefined" && $scope.profile_data != null) {
                // View relativa ao módulo
                $scope.current_view = "views/profile.html";                

                // Carrega a lista de cidades
                if($scope.profile_data.state_id != null) {
                    $scope.doGetCities($scope.profile_data.state_id);
                }

                if($scope.profile_data.picture_url) {
                    $scope.current_profile_image = $scope.base_url + "pictures/profile/" +  $scope.profile_data.picture_url; 
                }   
            }
        }
    }

    // Displays the save user form
    $scope.showSaveUser = function() {
        // Logica do modulo
        $scope.bar_title = "Cadastro";
        var action = "cadastro";
        if($scope.current_action != action) {
            window.closeMenu();
            // View relativa ao módulo
            $scope.current_view = "views/cadastro.html";
        }        
    }

    // Save an user
    $scope.doSaveUser = function() {
        try {
            var params = {};
            params.service = "user";
            params.action = "save";

            var email = $("#cad_email").val();
            var password = $("#cad_password").val();
            var passconf = $("#cad_password_conf").val();

            if(password != passconf) {
                throw "Senha e confirmação não conferem.";
            }            
            if($.trim(email) == "") {
                throw "Por favor preencha o email.";
            }
            
            if(!validateEmail($.trim(email))) {
                throw "Opa! Email invalido.";                
            }

            if($.trim(password) == "") {
                throw "Por favor preencha a senha.";
            }
            
            var data = {"user_email": email, "user_password": password};

            $scope.ws_post_request(params, data, "save_user");
        } catch(e) {
            alert(e);
        }
    }

    // Sends security code to email
    $scope.lostMypass = function() {
        try {
            var email = $("#login_email").val();
            if($.trim(email) == "") {
                throw "Por favor preencha o email.";
            }            
            if(!validateEmail($.trim(email))) {
                throw "Opa! Email invalido.";                
            }
            
            var params = {};
            params.service="user";
            params.action="recover-pass";
            params.email=String(email);

            $scope.ws_get_request(params, "lost_my_mass");
        } catch(e) {
            alert(e);
        }
    }

    // Update users pass
    $scope.updatePass = function() {
        $scope.current_action = 'update-pass';
        $scope.bar_title = "Redefinir Senha";
        try {            
            var remind_email = $("#remind_email").val();
            var remind_code = $("#remind_code").val();
            var remind_pass = $("#remind_pass").val();
            var remind_pass_conf = $("#remind_pass_conf").val();

            if($.trim(remind_code) == "") {
                throw "Insira o código enviado para o seu email: " + String(remind_email);
            }
            if($.trim(remind_pass) == "") {
                throw "Por favor preencha a senha.";
            }
            if(remind_pass != remind_pass_conf) {
                throw "Senha e confirmação não conferem.";
            }

            // Criptografa o password
            var new_pass = CryptoJS.MD5(remind_pass);

            var params = {};
            params.service = "user";
            params.action = "recover-pass";
            params.email = String(remind_email);
            params.remind_code = String(remind_code);
            params.new_pass = String(new_pass);

            $scope.ws_get_request(params, "update_pass");
        } catch(e) {
            alert(e);
        }
    }

    // Displays the feedback form
    $scope.doActionFeedback = function() {
        $scope.bar_title = "Comente";
        var action = "feedback";
        if($scope.current_action != action) {
            window.closeMenu();

            $scope.current_action = action;

            // View relativa ao módulo
            $scope.current_view = "views/feedback.html";
        }
    }

    // Get the Ranking top 10
    $scope.actionTop10 = function(system_id) {
        var action = "top10";
        $scope.bar_title = "Top 10 - Gamers";
        $scope.current_action = action;
        
        window.closeMenu();

        // Modifica o systema
        localStorage.setItem('system_id', system_id);

        var params = {};
        params.system_id = system_id;
        params.service="user"
        params.action="top-flags"
        params.current_view = "views/top-10.html";
        params.save_navigation = 1;
        params.display_messages = 1;

        $scope.ws_get_request(params, "ranking_top_10");
        $scope.current_view = params.current_view;
    }

    // Get friend list or search
    $scope.getFriends =  function(search, only_friend) {
        $scope.menu_type="burger";
        $scope.loading=true;

        $scope.bar_title = "Amigos";
        $scope.current_action = "get_friends";        
        window.closeMenu();

        var params = {};
        params.friend = false;
        params.service = "social";
        params.action = "list-people";
        params.user_id = String($scope.user_id);
        params.current_view = "views/friends.html";
        params.save_navigation = 1;
        params.limit = "0,10";

        // If receive search param
        if(typeof only_friend != "undefined") {
           params.only_friend =1;
        } else {
            params.only_friend =0;
        }

        if(typeof search != "undefined") {
           params.search = search;
        }

        //console.log(params);

        $scope.ws_get_request(params, "get_friends");
        $scope.current_view = params.current_view;
    };

    $scope.moreFriends = function() {
        $scope.bar_title = "Amigos";
        $scope.current_action = "more_friends";        
        window.closeMenu();

        var params = {};
        params.service = "social";
        params.action = "list-people";
        params.user_id = String($scope.user_id);
        params.current_view = "views/friends.html";
        params.save_navigation = 1;
        var limit1 = 0;
        var limit2 = 10;
        params.current_view = "views/friends.html";
        if($scope.hadougamers.length > 0) {
            limit1 = $scope.hadougamers.length-1;
        }
        params.limit = limit1 + "," + limit2;

        // If receive search param
        if(typeof $scope.params.only_friend != "undefined") {
           params.only_friend = $scope.params.only_friend;
        }

        if(typeof search != "undefined") {
           params.search = search;
        }

        $scope.ws_get_request(params, "more_friends");
        $scope.current_view = params.current_view;        
    }

    // Search trigger
    $scope.searchDebounce = function(word) {
        window.typingTimer;
        var doneTypingInterval = 400;
        clearTimeout(window.typingTimer);
        window.typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }

    // Search games
    $scope.doSearchGames = function(search) {
        console.log("Searching " + String(search));

        $scope.loading = true;
        $scope.gameList = [];
        $scope.controlId = [];        
        
        var params = {};
        params.user_id=String($scope.user_id);
        params.service='game';
        params.action = 'list';
        params.system_id = $scope.params.system_id;
        params.search = String(search).trim();
        params.current_view = "views/game-list.html";

        $scope.ws_get_request(params, "list_games");
        $scope.current_view = params.current_view;
    }


    // Get the Friend game list
    $scope.getFriendGames = function(friend, flag) {
        $scope.current_action = 'list';
        
        $scope.loading = true;
        $scope.menu_type = "arrow";
        $scope.friend = friend;
        window.closeMenu();

        params = {};
        params.service = "game";
        params.action = "list";
        params.flag = flag;
        params.system = 0;
        params.user_id = String(friend.user_id);
        params.only_owner_sell=1;
        params.current_view = "views/game-list.html";
        
        // Cleaning the list
        $scope.gameList = [];
        $scope.controlId = [];

        $scope.ws_get_request(params, "list_games");
        $scope.current_view = params.current_view;      
    }

    $scope.inviteFriends = function(search) {
        try {
            $scope.loading = true;
            $scope.current_action = "friends";
            window.closeMenu();
            if(!validateEmail($.trim(search))) {
                throw "Email do amigo inválido.";
            }

            var params = {};
            params.only_friend = $scope.params.only_friend;
            params.service = "social";
            params.action = "invite-friend";
            params.friend_email = String(search);
            params.user_id = String($scope.user_id);
            params.current_view = "views/friends.html";

            $scope.ws_get_request(params, "invite_friends");
        } catch(e) {
            alert(e);
            $scope.loading = false;
        }
    }

    // Display all game list
    $scope.doGetAllGames = function() {
        // Logica do modulo
        var action = "allgames";
        if($scope.current_action != action) {
            $scope.loading = true;
            $scope.current_action = action
            $scope.gameList = [];
            $scope.controlId = [];

            $scope.params.limit = "0,10";
            $scope.params.user_id = $scope.user_id;
            $scope.params.system = $scope.system_id;
            $scope.params.search = null;
            $scope.params.flag = null;

            $("#bt-favorite").removeClass("bt-favorite-enabled").addClass("bt-favorite");
            $("#bt-have").removeClass("bt-have-enabled").addClass("bt-have");
            $("#bt-watch").removeClass("bt-watch-enabled").addClass("bt-watch");
            $("#bt-all").addClass("bt-all-enabled");

            // faz a busca pela lista
            $scope.actionList();
        }
    }

    // Create or destroy an friendship
    $scope.friendship = function(friend_id, friend_name) {
        try {
            window.closeMenu();
            
            var element = $("#bt-friend-" + String(friend_id));
            var bt_action = element.attr("data-friend-action");

            var params = {};
            params.service = "social";
            params.user_id = String($scope.user_id);
            params.friend_id = String(friend_id);
            params.only_friend = $scope.params.only_friend;
            params.limit = $scope.params.limit;

            if(bt_action == "remove") {
                var remove = confirm("Desfazer amizade com " + String(friend_name) + "?");
                if (remove != true) {
                    return;
                }
                element.removeClass("active");
                params.action = "remove-friend";                
                element.attr("data-friend-action", "add");
            } else {
                var add = confirm(String(friend_name) + " receberá um email sobre esta ação.");
                if (add != true) {
                    return;
                }                
                element.addClass("active");
                params.action = "add-friend";
                element.attr("data-friend-action", "remove");
            }
            
            $scope.ws_get_request(params, "friendship");            
        } catch(e) {
            alert(e);
        }
    }

    // Add an flag
    $scope.addFlag = function(game_id, game_flag, game_value) {
        var params = {};
        params.service="user";
        params.action="flag-" + String(game_flag);
        params.user_id= String($scope.user_id);
        params.game_id= game_id;
        params.flag= $scope.params.flag;
        params.game_flag=game_flag;
        params.display_messages = 0; // Dont display alerts
        
        if(typeof game_value !== 'undefined') {
            params.game_value = game_value;
        }
        
        $scope.ws_get_request(params, "add_flag");
    }

    // Remove an flag
    $scope.removeFlag = function(game_id, game_flag) {
        var params = {};
        params.service="user";
        params.action="remove-flag";
        params.user_id= $scope.user_id;
        params.game_id= game_id;
        params.flag= $scope.params.flag;
        params.game_flag=game_flag;
        params.display_messages = 0; // Dont display alerts

        $scope.ws_get_request(params, "remove_flag");        
    }

    // Turn on/off an flag
    $scope.toggleFlag = function (element_id) {
        var element = $("#" + element_id);
        var parts = String(element_id).split("-");
        var flag = parts[0];
        var game_id = parts[1];
        var status = element.attr("data-flag-status");

        if(status == "Y") {
            $scope.removeFlag(game_id, flag);
            element.attr("data-flag-status", "N");
        }
        if(status == "N") {
            if(flag == "sell") {
                var game_value = prompt("Por quanto deseja vender? (R$)");
                if (game_value == null) {
                    return;
                }
                if(String(game_value).trim() == "") {
                    game_value = "Valor a combinar.";
                }
            }

            $scope.addFlag(game_id, flag, game_value);
            element.attr("data-flag-status", "Y");
        }
        element.toggleClass("button-enabled");
    }

    // Disable friend reference
    $scope.clearFriend = function() {
        $scope.menu_type = "burger";
        $scope.friend = false;
    }

    // State list
    $scope.doGetStates = _.memoize(function() {
        var params = {};
        params.service="address";
        params.action="list-states";
        $scope.ws_get_request(params, "get_states");
    });

    // City list
    $scope.doGetCities = function(state_id) {
        if(state_id != 0) {
            var params = {};
            params.service="address";
            params.action="list-cities";
            params.state_id= state_id;
            $scope.ws_get_request(params, "get_cities");
        }
    }

    $scope.sellerPopup = function(game_id) {
        var params = {};
        params.service = "game";
        params.action = "list-sellers";
        params.game_id = String(game_id);
        params.user_id = String($scope.user_id);
        
        $scope.ws_get_request(params, "seller_popup");
    }

    $scope.closeSellerPopup = function() {
       $scope.popupSellers = false;
    }

    $scope.wantThisGame = function(user_id, sell_data) {
        var sendmail = confirm("Enviar email sobre '" + String(sell_data.game_title) + "'  para " + String(sell_data.nickname) + "?");
        if (sendmail != true) {
            return;
        }        

        var params = {};
        params.service = "game";
        params.action = "contact-seller";
        params.flag_id = String(sell_data.flag_id);
        params.buyer_id = String(user_id);
        params.seller_id = String(sell_data.user_id);
        params.game_id = String(sell_data.game_id);
        params.game_value = String(sell_data.game_value);
        params.user_id = $scope.params.user_id || null;
        params.search = $scope.params.search || null;
        params.flag = $scope.params.flag || null;

        $scope.ws_get_request(params, "want_this_game");
    }

    $scope.notifications = function(event) {
        console.log(event.payload.ws_action);
        if(event.payload.ws_action == "add-chat-message") {
            if($("#chat-messages").length > 0 && $scope.friend && String($scope.friend.user_id) == String(event.payload.sender_id) ) {
                var data = {};
                data.message_id = event.payload.message_id;
                data.sender_id = event.payload.sender_id;
                data.receiver_id = event.payload.receiver_id;
                data.origin = "theirs";
                data.message = event.payload.chat_message;
                data.date_time = event.payload.date_time;
                $scope.addChatMessage(data);
            }
        }
        if (event.foreground) {
            // App Opened
            console.log("App opened.");
        } else {
            // App open nbecause we tap on notification tray
            if (event.coldstart) {
                // Coldstart
                console.log("App coldstart.");
                if(event.payload.ws_action == "add-chat-message") {
                    //$scope.getFriends(String(event.payload.sender_nickname));
                    $scope.actionChat(event.payload.friend, "burger", "0,5");
                }
            } else {
                // Background
                console.log("App background.");
                if(event.payload.ws_action == "add-chat-message") {
                    //$scope.getFriends(String(event.payload.sender_nickname));
                    $scope.actionChat(event.payload.friend, "burger", "0,5");
                }
            }
        }     
    }

});

function request_callback(data) {
    try {
        var controller = window.angular.element(document.getElementById('controller'));
        if(data.status == "NOT_OK") {
            controller.scope().popupSellers = false;
            var display_messages = parseInt(data.request.display_messages);
            if(display_messages != 0) {
                //alert(data.message);
                controller.scope().error_message = data.message;
            }

            if(data.request.requestType == 'do_login' || 
              data.request.requestType == "save_user" || 
              data.request.requestType == "save_profile" || 
              data.request.requestType == "send_feedback" ) {
              alert(data.message);
            }


            //console.log(data.request);
        } else {
            action_data(data);
        }
    } catch(e) {
        console.log(e);
    }
}

// Do something according received data
function action_data(data) {
    switch(data.request.requestType) {
        case "list_games":
            if(data.data instanceof Array) {
                controller.scope().addToGameList(data.data);
                $('body,html').animate({scrollTop:0},600);
                
                // 3 Minutes cache no flag cache
                if(!data.request.flag) {
                    // Save in cache
                    window.CacheControl.save_cache(data.request, data.data, 180);
                }
            }
        break;  
        case "more_games":
            if(data.data instanceof Array) {
                if ( $( "#game-list" ).length > 0) {
                    controller.scope().addToGameList(data.data);
                    // Save in cache
                    window.CacheControl.save_cache(data.request, data.data, 30);
                }
            }
        break;
        case "get_friends":
            controller.scope().hadougamers = [];
            controller.scope().FriendControlId = [];
            controller.scope().addToFriendList(data.data);
            // Only cache if no friends (5 minutes)
            if(data.request.only_friend == 0) {
                window.CacheControl.save_cache(data.request, data.data, 300);
            }
            $('body,html').animate({scrollTop:0},600);
        break;                
        case "more_friends":
            if(data.data instanceof Array) {
                if ( $( "#hadougamer-list" ).length > 0) {
                    controller.scope().addToFriendList(data.data);
                    // Only cache if no friends (5 minutes)
                    if(data.request.only_friend == 0) {
                        window.CacheControl.save_cache(data.request, data.data, 300);
                    }
                }
            }
        break;                
        case "get_states":
            if(data.data instanceof Array) {
                controller.scope().states = data.data;
                // 30 days cache for states
                window.CacheControl.save_cache(data.request, data.data, 2592000);
            }
        break;
        case "get_cities":
            if(data.data instanceof Array) {
                controller.scope().cities = data.data;
                // 30 days cache for cities
                window.CacheControl.save_cache(data.request, data.data, 2592000);                
            }
        break;
        case "save_profile":
            alert("Perfil editado com sucesso.");
            var profile_data = data.data;
            localStorage.setItem('profile_data', JSON.stringify(profile_data));
            controller.scope().profile_data = profile_data;
            $("#profile_id").val(data.data.profile_id);
            controller.scope().actionProfile();
        break;
        case "ranking_top_10":
            controller.scope().topList = data.data;
            // Save in cache
            window.CacheControl.save_cache(data.request, data.data, 300);
        break;
        case "send_feedback":
            alert("Obrigado por sua mensagem. :)");
        break;
        case "use_terms":
            controller.scope().useTerms = controller.injector().get('$sce').trustAsHtml(data.data.terms_text);
            // Save in cache
            window.CacheControl.save_cache(data.request, data.data, 86400);
        break;
        case "invite_friends":
            alert("Amigo convidado com sucesso.");
            $("#hadougamer_name").val("");
        break;
        case "add_flag":
            console.log("Flag added.");
        break;
        case "remove_flag":
            console.log("Flag removed.");
        break;
        case "do_login":
            var profile_data = data.data;
            // Saves profile data
            localStorage.setItem('profile_data', JSON.stringify(profile_data));
            controller.scope().profile_data = profile_data;

            // saves user_id
            localStorage.setItem('user_id', data.data.user_id);
            controller.scope().user_id = data.data.user_id;
            
            location.href = "index.html";
        break;
        case "save_user":
            // Exibe a messagem de sucesso
            alert("Seja bem vindo ao Hadougame! Hora de organizar a coleção.");
            controller.scope().user_id = data.data.user_id;
            // Manda o usuario para a list
            localStorage.setItem('user_id', data.data.user_id);
            localStorage.setItem('profile_data', JSON.stringify(data.data.profile_data));

            location.href = "index.html";
        break;
        case "lost_my_mass":
            controller.scope().remind_email = data.data.user_email;
            controller.scope().current_view = "views/lost-pass.html";
            alert("Enviamos um código de segurança para seu email.");
        break;
        case "update_pass":
            alert("Senha alterada com sucesso.");
            controller.scope().actionLogin();
        break;
        case "friendship":
            if(data.request.action == "remove-friend") {
                //var message = "Amigo removido com sucesso.";                        
                
                // If in a friendlist remove from screen
                if(data.request.only_friend == 1) {
                    controller.scope().hadougamers = controller.scope().hadougamers.filter(
                        function (gamer) { return gamer.user_id !== data.request.friend_id; }
                    );                            
                }

            }
            if(data.request.action == "add-friend") {
                //var message = "Amigo adicionado com sucesso.";
            }
            //alert(message);
        break;
        case "seller_popup":
            controller.scope().listSellers = data.data;
            controller.scope().popupSellers = true;
        break
        case "want_this_game":
            alert("Email enviado ao vendedor. Aguarde o contato.");
            controller.scope().popupSellers = false;
        break;
        case "save_gcm":
            controller.scope().gcm_id = data.request.gcm_id;
            localStorage.setItem('gcm_id', data.request.gcm_id);
        break;
        case "chat_message_list":
            if(data.data instanceof Array) {
                controller.scope().addListChatMessage(data.data);
            }
        break;                
        case "send_chat_message":
            data.data.origin = "yours";
            controller.scope().chat_messages[data.request.message_index] = data.data;
        break;
    }

    // If action setted an view save params on history
    if(parseInt(data.request.save_navigation) == 1) {
        controller.scope().navigation_store(data.request);
    }

    //console.log(controller.scope().navigation);
    controller.scope().loading = false;
}

                

// Trigger on scroll
function gameListScroll(){
    if(angular.element(document.getElementById('controller')).scope().current_view == "views/game-list.html") {
        angular.element(document.getElementById('controller')).scope().moreGames();
    } else {
        console.log("gameListScroll chegou tarde.");
    }
    clearTimeout(window.scrollTimer);
}

function friendListScroll(){
    if(angular.element(document.getElementById('controller')).scope().current_view == "views/friends.html") {
        angular.element(document.getElementById('controller')).scope().moreFriends();
    } else {
        console.log("friendListScroll chegou tarde.");
    }
    clearTimeout(window.scrollTimer);
}

window.onscroll = function(ev) {
    if ( $( "#game-list" ).length ) {
        window.scrollTimer; // IDdo timer
        var doneScrollInterval = 300; // Tempo de delay em milisegundos

        clearTimeout(window.scrollTimer); // Limpa o identificador
        window.scrollTimer = setTimeout(gameListScroll, doneScrollInterval); // Cria um novo identificador
    }

    if ( $( "#hadougamer-list" ).length ) {
        window.scrollTimer; // IDdo timer
        var doneScrollInterval = 300; // Tempo de delay em milisegundos

        clearTimeout(window.scrollTimer); // Limpa o identificador
        window.scrollTimer = setTimeout(friendListScroll, doneScrollInterval); // Cria um novo identificador
    }    
};

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

//user is "finished typing," do something
function doneTyping () {
    //do something
    var word = $("#search-field").val();
    
    clearTimeout(window.typingTimer); // Limpa o identificador
    angular.element(document.getElementById('controller')).scope().doSearchGames(word);
}

function openMenu(){
    if($("#sidebar").hasClass("opened")) {
        $("#sidebar").attr("class", "sidebar");
    } else {
        $("#sidebar").attr("class", "sidebar opened");
    }
}

function closeMenu(){
    $("#sidebar").attr("class", "sidebar");
}

function disabledFlagButtons() {
    var flags = ["have", "favorite", "watch", "sell","all"];
    angular.element(document.getElementById('controller')).scope().params.flag = null;
    for(var i=0; i<flags.length; i++) {
        $("#bt-" + flags[i]).attr("class", "bt-header bt-" + flags[i]);
    }
}

// Receive and handle URL from custom url plugin
function handleOpenURL(url) {
  console.log(url);
}